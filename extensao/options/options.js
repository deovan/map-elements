
let selectLanguage = document.getElementById('select-language');
let checkboxShouldLabelsSearch = document.getElementById('checkbox-should-search-labels');

chrome.storage.sync.get(['language', 'shouldLabelsSearch'], function (result) {
    if (result != undefined && result != null && !jQuery.isEmptyObject(result)) {
        selectLanguage.value = result.language
        checkboxShouldLabelsSearch.checked = result.shouldLabelsSearch
    } else {
        chrome.storage.sync.set({ 'language': 1, 'shouldLabelsSearch': false }, function () {
            selectLanguage.value = 1;
            checkboxShouldLabelsSearch.checked = false;
        })
    }
});

selectLanguage.onchange = function (ev) {
    if (!!ev) {
        chrome.storage.sync.set({ 'language': ev.target.value }, function (result) { });
    }
}

checkboxShouldLabelsSearch.onclick = function () {
    chrome.storage.sync.set({ 'shouldLabelsSearch': checkboxShouldLabelsSearch.checked }, function (result) { });
}

// let changeColor = document.getElementById('changeColor');
let inputPath = document.getElementById('input-path');
let btnBuscar = document.getElementById('buscar-elementos');
let btnBuscarLabels = document.getElementById('buscar-labels');
let btnLimpar = document.getElementById('limpar-elementos');
let btnCopiar = document.getElementById('copy-elementos');
let spaceCode = document.getElementById('code-list');
let alertCopiadoSucesso = document.getElementById('alert-copiado-sucesso');
let alertNenhumResultado = document.getElementById('alert-nenhum-resultado');

let language;
chrome.storage.sync.get(['language', 'shouldLabelsSearch'], function (result) {
  if (result != undefined && result != null && !jQuery.isEmptyObject(result)) {
    language = result.language
    btnBuscarLabels.hidden = !result.shouldLabelsSearch
  } else {
    chrome.storage.sync.set({ 'language': 1, 'shouldLabelsSearch': false }, function () {
      language = 1;
      btnBuscarLabels.hidden = true;
    })
  }
});

btnLimpar.onclick = function () {
  spaceCode.innerHTML = ""
  btnLimpar.hidden = true;
  btnCopiar.hidden = true;
}

btnBuscar.onclick = function () {
  btnLimpar.click();
  var valor = !!inputPath.value ? inputPath.value : '*'
  //We have permission to access the activeTab, so we can call chrome.tabs.executeScript:
  chrome.tabs.executeScript({
    code: 'var language = ' + language + '; var teste = "' + valor + '"; ' + '(' + getElements + ')();' //argument here is a string but function.toString() returns function's code
  }, (results) => {
    if (!results[0]) {
      btnLimpar.hidden = true;
      btnCopiar.hidden = true;
      alertNenhumResultado.hidden = false;
      setTimeout(() => {
        alertNenhumResultado.hidden = true;
      }, 5000);
    } else {
      btnLimpar.hidden = false;
      btnCopiar.hidden = false;
      if (language == 4) {
        var code = document.createElement('code');
        // p.className = 'texto-result';
        code.innerText = JSON.stringify(results[0], null, 2);
        spaceCode.append(code);
        spaceCode.append(document.createElement("br"));
      } else {
        //Here we have just the innerHTML and not DOM structure
        results[0].forEach(element => {
          var p = document.createElement('span');
          p.className = 'texto-result';
          p.innerText = element;
          spaceCode.append(p);
          spaceCode.append(document.createElement("br"));
        });
      }


    }
  });

}

btnBuscarLabels.onclick = function () {
  var valor = !!inputPath.value ? inputPath.value : '*'
  btnLimpar.click();
  //We have permission to access the activeTab, so we can call chrome.tabs.executeScript:
  chrome.tabs.executeScript({
    code: 'var teste = "' + valor + '"; ' + '(' + getLabels + ')();' //argument here is a string but function.toString() returns function's code
  }, (results) => {
    if (!results[0]) {
      btnLimpar.hidden = true;
      btnCopiar.hidden = true;
      alertNenhumResultado.hidden = false;
      setTimeout(() => {
        alertNenhumResultado.hidden = true;
      }, 5000);
    } else {
      btnLimpar.hidden = false;
      btnCopiar.hidden = false;
      //Here we have just the innerHTML and not DOM structure
      results[0].forEach(element => {
        var textnode = document.createTextNode(element); // Create a text node
        spaceCode.append(textnode);
        spaceCode.append(document.createElement("br"));
      });
    }
  })
}

btnCopiar.onclick = function copyFun() {
  /* Get the text field */

  /* Select the text field */
  var range = document.createRange();
  range.selectNode(spaceCode);
  window.getSelection().removeAllRanges(); // clear current selection
  window.getSelection().addRange(range); // to select text
  document.execCommand("copy");
  window.getSelection().removeAllRanges(); // to deselect

  /* Alert the copied text */
  alertCopiadoSucesso.hidden = false;
  setTimeout(() => (alertCopiadoSucesso.hidden = true), 5000)
}

function getElements() {
  //You can play with your DOM here or check URL against your regex
  var allElement = document.querySelector(teste).getElementsByTagName("*");

  var lista = [];
  let objetoJson = {};

  for (var i = 0; i < allElement.length; i++) {

    if (!!allElement[i].id) {

      var variableName = allElement[i].tagName.replace('MAT-', '').toLowerCase() + ' ' + allElement[i].id.normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/[!.,?@#$%/;:^()&*]/g, "");;

      var variableCamelCase = variableName.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
        return index == 0 ? word.toLowerCase() : word.toUpperCase();
      }).replace(/\s+/g, '');
      var nome = '';
      switch (language) {
        case 1:
          nome = 'private By ' + variableCamelCase.replace(new RegExp('-', 'g'), '') + ' = By.id("' + allElement[i].id + '");'
          break;
        case 2:
          nome = `@FindBy(id = "${allElement[i].id}")\n private WebElement ${variableCamelCase.replace(new RegExp('-', 'g'), '')};`
          break;
        case 3:
          nome = `\${${variableCamelCase.replace(new RegExp('-', 'g'), '_').toUpperCase()}}\t\tid:${allElement[i].id}`;
          break;
        case 4:
          objetoJson[variableCamelCase.replace(new RegExp('-', 'g'), '')] = `#${allElement[i].id.replace(new RegExp(':', 'g'), '\\:')}`
          break;
      }

      lista.push(nome);

    } else if (!!allElement[i].getAttribute("name")) {

      var variableName = allElement[i].tagName.replace(new RegExp('SRO-TOPIC-SEPARATOR', 'g'), "separator") + ' ' + allElement[i].getAttribute("name");
      variableName = variableName.toLowerCase().normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/[!.,?@#$%/;:^&()*]/g, "");;

      var variableCamelCase = variableName.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
        return index == 0 ? word.toLowerCase() : word.toUpperCase();
      }).replace(/\s+/g, '');

      var nome = '';
      switch (language) {
        case 1:
          nome = `private By ${variableCamelCase.replace(new RegExp('-', 'g'), '')} = By.name("${allElement[i].getAttribute("name")}");`
          break;
        case 2:
          nome = `@FindBy(name = "${allElement[i].getAttribute("name")}")\n private WebElement ${variableCamelCase.replace(new RegExp('-', 'g'), '')};`
          break;
        case 3:
          nome = `\${${variableCamelCase.replace(new RegExp('-', 'g'), '_').toUpperCase()}}\t\tname:${allElement[i].getAttribute("name")}`;
          break;
        case 4:
          objetoJson[variableCamelCase.replace(new RegExp('-', 'g'), '')] = `[name='${allElement[i].getAttribute("name")}']`;
          break;
      }

      lista.push(nome)
    }
  }

  return language !== 4 ? (lista.length > 0 ? lista : null) : objetoJson;
}

function getLabels() {
  //You can play with your DOM here or check URL against your regex
  var allElement = document.querySelector(teste).getElementsByTagName("label");

  var listaLabel = [];

  for (var i = 0; i < allElement.length; i++) {

    if (!!allElement[i].getAttribute("class") && (allElement[i].getAttribute("class").includes("suffix") || allElement[i].getAttribute("class").includes("mat-radio-label"))) {

    } else {
      var variableName = allElement[i].innerHTML.normalize('NFD').trim()
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/[!.,?@#$%/;:^&()*]/g, "-");

      var variableCamelCase = variableName.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
        return index == 0 ? word.toLowerCase() : word.toUpperCase();
      }).replace(/\s+/g, '');

      var nome = 'private String ' + variableCamelCase.replace(new RegExp('-', 'g'), '') + ';';
      listaLabel.push(nome)
    }
  }

  console.log('lista: ', listaLabel)
  return listaLabel.length > 0 ? listaLabel : null;
}